	@cross_origin(origin='*')
	@jwt_required
	def delete(self):
		""" Es usado para desasignar a un cliente de la cartera de un ejecutivo """
		
		id_cliente = request.args.get('idcliente', None)

		from leasing import db
		engine = db.get_engine()
		conn = engine.connect()
		trans = conn.begin()

		try:
		
			if not id_cliente or int(id_cliente) == 0:
				return make_response(jsonify({"codigo": 1201, "msg": "Parametro idcliente no es correcto"}), 400)

			id_cliente = int(id_cliente)

			from app_cliente_cartera.main.models import ClienteCarteraModel
			from app_cripto.main.controllers import ControllerCriptor

			contm, cy = ClienteCarteraModel(), ControllerCriptor()

			current_user = get_jwt_identity()
			current_user = current_user[2: -1]
			iduser = cy.decrypt(current_user)
			iduser = str(iduser)[2 : -1]
			iduser = int(iduser)
			mensaje = 'Clientes asignados'

			# if len(clit) > 0: 
			# 	contm.eliminar_cartera_cliente_id( conn, id_eje, clit)
			# 	for row in clit: contm.grabar_asig_ejecutivo_cliente_web( conn, iduser, id_eje, row )
			# else:
			# 	return make_response(jsonify({"msg": "No hay registros de clientes"}), 400)	

			trans.commit()
			conn.close()

			return make_response(jsonify({"state" : 'success', "msg" : mensaje}), 200)
		except ValueError:
			trans.rollback()
			return make_response(jsonify({"codigo": 1202, "msg": "Parametro idcliente no es correcto"}), 400)
		except Exception as inst:
			trans.rollback()
			return make_response(jsonify({"msg": "Por favor intente nuevamente", "error": inst.args}), 400)
		finally:
			conn.close()