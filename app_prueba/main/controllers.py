from flask_restful import Resource, reqparse
from flask import Flask, jsonify, request, make_response
from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required, 
jwt_refresh_token_required, get_jwt_identity, get_raw_jwt)
from datetime import datetime, date, time, timedelta
import calendar
import requests
import json
import sys

from flask_cors import CORS, cross_origin

class ControllerPrueba2(Resource):

	@cross_origin(origin='*')
	#@jwt_required
	def get(self):

		from run import db
		engine = db.get_engine(bind='db1')
		conn = engine.connect()
		trans = conn.begin()

		try:

			from .models import PruebaModel
			bm = PruebaModel()
			coti_admcliente_r = bm.consultar_boleta_admcliente(conn)

			if coti_admcliente_r.rowcount > 0:
				for row in coti_admcliente_r:
					id_pp = row["id"]
					nombre = row["nombre"]
			else:
				return make_response(jsonify({"msg": 'La boleta no existe'}), 400)

			trans.commit()
			conn.close()

			return make_response(jsonify({ 'state' : 'Successful', "nombre" : nombre }), 200)

		except Exception as inst:
			trans.rollback()
			return make_response(jsonify({"msg": "Please try again", "error": 'Error on line {}'.format(sys.exc_info()[-1].tb_lineno), "error2": inst.args }), 400)

		finally:
			conn.close()



