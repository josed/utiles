from run import db
from passlib.hash import pbkdf2_sha256 as sha256
from sqlalchemy import text
from sqlalchemy import create_engine
from flask import Flask, jsonify, request, make_response

class PruebaModel(db.Model):

	__abstract__ = True
	__bind_key__ = 'db1'

	def consultar_boleta_admcliente(cls, conn):
		sql = text("select id, nombre from usuario;")
		trans = conn.begin()
		result = conn.execute(sql)
		trans.commit()
		return result