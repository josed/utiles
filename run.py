# !/usr/bin/env python
# -*- coding:utf-8 -*-
from flask import Flask
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_jwt_extended import JWTManager

from app_user.main.controllers import ControllerCliente, ControllerUserTocken, ControllerClienteAccess, ControllerPrueba, ControllerClientexId, ControllerUsuarioWeb, ControllerInfoBasico, ControllerLogoTemplate, ControllerLogoUpload, ControllerCertdata, ControllerCertUpload, ControllerCinfUsuxId, ControllerCdatauser, ControllerCUsupasWeb

from app_user.main.controllersuser import ControllerUserValid, ControllerUserLogout
from app_perfil.main.controllers import Perfil
from app_utiles.main.controllers import ControllerDepartamento, ControllerProvincia, ControllerDistrito, ControllerMoneda, ControllerTipoContacto, ControllerTipoCambio, ControllerPaquetes, ControllerUnidaSunat, ControllerTemplatefac, ControllerTemplatefacListar, ControllerTemplatefacxId, ControllerTemplatefacxCliente, ControllerDashboard

from app_cotizacion.main.controllers import ControllerCotizacion, ControllerCotizacionListar, ControllerCotizacionFiltrar, ControllerCotizacionBusqueda, ControllerCotizacionBusquedaPagina, ControllerCotizacionPreview, ControllerCotizacionFiltroTotal, ControllerCotizacionSeguimiento #Said 21/03/2018
from app_factura.main.controllers import ControllerFactura, ControllerFacturaSeguimiento, ControllerFacturaBusqueda, ControllerFacturaBusquedaPagina, ControllerFacturaPreview #Said 22/03/2018
from app_boleta.main.controllers import ControllerBoleta, ControllerBoletaSeguimiento, ControllerBoletaBusqueda, ControllerBoletaBusquedaPagina, ControllerBoletaPreview #Said 22/03/2018
from app_notacredito.main.controllers import ControllerNotaCredito, ControllerNotaCreditoListar, ControllerNotaCreditoFiltrar, ControllerNotaCreditoAnular, ControllerNotaCreditoSeguimiento, ControllerNotaCreditoBusqueda, ControllerNotaCreditoBusquedaPagina, ControllerNotaCreditoPreview #Said 23/03/2018
from app_notadebito.main.controllers import ControllerNotaDebito, ControllerNotaDebitoListar, ControllerNotaDebitoFiltrar, ControllerNotaDebitoAnular, ControllerNotaDebitoSeguimiento, ControllerNotaDebitoBusqueda, ControllerNotaDebitoBusquedaPagina, ControllerNotaDebitoPreview #Said 23/03/2018
from app_percepcion.main.controllers import ControllerPercepcion, ControllerPercepcionAnular, ControllerPercepcionSeguimiento, ControllerPercepcionBusqueda, ControllerPercepcionPreview #Said 26/03/2018
from app_retencion.main.controllers import ControllerRetencion, ControllerRetencionSeguimiento, ControllerRetencionBusqueda, ControllerRetencionPreview #Said 27/03/2018
from app_guiaremision.main.controllers import ControllerGuiaRemision, ControllerGuiaRemisionSeguimiento, ControllerGuiaRemisionBusqueda, ControllerGuiaRemisionPreview, ControllerDireccionesListar, ControllerDocumentosRelacionados #Said 27/03/2018
# from app_guiatransporte.main.controllers import ControllerGuiaTransporte, ControllerGuiaTransporteListar, ControllerGuiaTransporteFiltrar #Said 28/03/2018
from app_docbaja.main.controllers import ControllerDocBajaListar, ControllerDocReversionListar #Said 11/04/2018
from app_comunicacion.main.controllers import ControllerComunicacion, ControllerComunicacionListar, ControllerComunicacionDetListar, ControllerLoteListar #Said 12/04/2018
from app_resumenboleta.main.controllers import ControllerResumenBoleta, ControllerResumenBoletaListar, ControllerResumenBoletaDetListar, ControllerLoteResumenListar, ControllerPendientesResumenListar #Said 18/04/2018


#Menu Administracion
from app_vehiculo.main.controllers import ControllerVehiculo, ControllerVehiculoListar, ControllerVehiculoFiltrar  #Vehiculo - Said 13/03/2018
from app_conductor.main.controllers import ControllerConductor, ControllerConductorListar, ControllerConductorFiltrar  #Conductor - Said 13/03/2018
from app_condicionpago.main.controllers import ControllerCondicionPago, ControllerCondicionPagoListar, ControllerCondicionPagoFiltrar  #Condicion Pago - Said 13/03/2018
from app_perfiladm.main.controllers import ControllerPerfilAdm, ControllerPerfilAdmListar, ControllerPerfilAdmFiltrar  #PerfilAdm - Said 14/03/2018
from app_usuario.main.controllers import ControllerUsuario, ControllerUsuarioListar, ControllerUsuarioFiltrar, ControllerLocalesDisponibles  #Usuario - Said 14/03/2018
from app_umedida.main.controllers import ControllerUnidadMedida, ControllerUnidadMedidaListar, ControllerUnidadMedidaFiltrar #Unidad Medida - Said 15/03/2018, Add ControllerUnidadMedidaListar
from app_local.main.controllers import ControllerLocal, ControllerLocalListar, ControllerLocalListar2, ControllerLocalxId #Local - Said 16/03/2018
from app_producto.main.controllers import ControllerProducto, ControllerProductoListar, ControllerProductoxId #Producto/Servicio - Said 16/03/2018, Add ControllerProductoListar, ControllerProductoxId

from app_contacto.main.controllers import ControllerContacto, ControllerContactoListar, ControllerContactoxId, ControllerContactoValidarDocumento, ControllerContactoValidarEmail, ControllerDireccion, ControllerDireccionListar #Contacto - Said 16/03/2018, Add ControllerContactoListar, ControllerContactoxId

from app_asignacion.main.controllers import ControllerAsignacion, ControllerAsignacionListar, ControllerAsignacionComprobantes #Said 30/04/2018

#Utiles para los Comprobantes
from app_utiles_comprobantes.main.controllers import ControllerUtilContactoTipo, ControllerTipoAfectoIGV, ControllerUtilSerieCorrelativo, ControllerUtilProductoxMoneda, ControllerUtilUnidadMedida, ControllerComprobanteaModificar, ControllerTipoNotaCredito, ControllerTipoNotaDebito, ControllerTipoOperacion, ControllerUtilClientesConRUc # Said - 20/03/2018
from app_utiles_comprobantes.main.controllers import ControllerRegimenPercepcion, ControllerRegimenRetencion, ControllerMotivoTraslado, ControllerModalidadTraslado, ControllerTipoTransporte, ControllerComprobanteBaja, ControllerUtilCondicionPago, ControllerUtilPerfiles, ControllerUtilConsultarComprobante, ControllerUtilEnviarEmailComprobante, ControllerUtilEnviarEmailComprobanteEnv, ControllerUtilValidarSerieCorrelativo #Said - 27/03/2018

#Opciones Para los PerfilesAdm
from app_opcionesadm.main.controllers import ControllerOpcionesAdm # Said - 26/04/2018

#Acceso
from app_acceso.main.controllers import ControllerAcceso, ControllerReporte, ControllerListarClientes, ControllerListarUsuario # Said - 11/04/2018

#Descargar Archivos PDF, XML, CDR
from app_descargar.main.controllers import ControllerDescargarArhivo, ControllerDescargarArhivojose #Said - 07/05/2018

#Anulacion de Comprobantes
from app_anulacion.main.controllers import ControllerAnulacion #Said 07/05/2018

#Busqueda Avanzada
from app_busqueda.main.controllers import ControllerBusquedaAvanzada, ControllerBusquedaAvanzadaPagina #Said 09/05/2018

#recuperar contraseña
from app_emailcontrasenia.main.controllers import ControllerRecuperarContrasenia, ControllerValidaparamContrasenia, ControllerSaveparamContrasenia

#Reportes
from app_reporte.main.controllers import ControllerReportes, ControllerReportesUtilLocal, ControllerReportesUtilUsuario, ControllerRptFacturasFechaPago, ControllerReportesExcel #Said 22/05/2018

#pdf cotizacion
from app_cotizacion_pdf.main.controllers import * #BRUNO 22/05/2018
from app_dashboard.main.controllers import ControllerDashboard2 # Said 08/06/2018

#Simple - Para procesos manuales
from app_simple.main.controllers import ControllerClientexRuc # Said 14/06/2018

#Para crear excel
from app_excel.main.controllers import ControllerExecelPrueba # Said 02/07/2018

#Servicios
from app_servicios.main.controllers import ControllerServicioFactura, ControllerServicioBoleta, ControllerServicioNotaCredito, ControllerServicioNotaDebito, ControllerServicioGuiaRemision, ControllerServicioPercepcion, ControllerServicioRetencion # Said 12/07/2018

#Consulta Publica
from app_publica.main.controllers import * # Said 24/07/2018

#Cobranza
from app_cobranza.main.controllers import ControllerCobranzaListar, ControllerCobranzaPreview, ControllerCobranza, ControllerFormaPago, ControllerCobranzaNotificacion

#Prueba
from app_prueba.main.controllers import ControllerPrueba2

from flask_cors import CORS, cross_origin


from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_jwt_identity
)
from flask import Flask, jsonify, request, make_response

app = Flask(__name__, static_folder='/var/www/documentos')
CORS(app)


# Variables globales de IPs
# nom_servidor = 'ubuntu'
# ip_servidor = 'http://52.205.175.163' # Ubuntu
# ip_agente = 'http://localhost' # Ubuntu
nom_servidor = 'centos'
ip_servidor = 'http://172.31.8.246' # Centos
ip_agente = 'http://172.31.12.216' # Centos


######################## config database ############################

POSTGRES = {
    'user': 'postgres',
    'pw': 'jose',
    'db': 'facturacion',
    'host': 'localhost',
    'port': 5433
}


# app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:jose@192.168.21.121:5433/facturador_entel'
# app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://aws_simple:WtL58NtkqahUUAuJ@172.31.5.208:5432/facturador_entel'
# app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:postgres@localhost:5432/back_elena'
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:postgres@52.205.175.163:5432/facturador_entel'
app.config['SQLALCHEMY_BINDS'] = { 'db1' : 'postgresql://postgres:postgres@52.205.175.163:5432/db_jose' }
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = 'qQ1E23$$#ddDDmaAKZ23sdSS#%R'


db = SQLAlchemy(app)


######################## config Access Tocken ########################
UPLOAD_FOLDER = '/var/www/documentos'
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])


# app.config['JWT_TOKEN_LOCATION']        = 
app.config['JWT_SECRET_KEY']            = '1Aq24$#(ffdDSSD$$!234VDfdDZZX01'
app.config['JWT_ALGORITHM']             = 'HS256'
app.config['JWT_ACCESS_TOKEN_EXPIRES']  = False # 15 MINUTOS ES POR DEFAULT
# app.config['JWT_ACCESS_TOKEN_EXPIRES']  = 25
# app.config['JWT_REFRESH_TOKEN_EXPIRES'] = False # 30 DIAS TOCKEN DE REFRESH
# app.config['JWT_REFRESH_TOKEN_EXPIRES'] = 25
# app.config['CORS_HEADERS'] = 'Content-Type'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER 
# app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
# app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024

jwt = JWTManager(app)

api = Api(app)


# jwt._set_error_handler_callbacks(app)

# app.config['JWT_BLACKLIST_ENABLED'] = True
# app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']

# @jwt.token_in_blacklist_loader
# def check_if_token_in_blacklist(decrypted_token):
#   # jti = decrypted_token['jti']
#   jti = decrypted_token['access_token']
#   # return RevokedTokenModel.is_jti_blacklisted(jti)
#   from app_user.main.models import RevokedTokenModel
#   return RevokedTokenModel.is_jti_blacklisted(jti)

@jwt.user_claims_loader
def add_claims_to_access_token(identity):
    return {
        'user_ident': identity,
        'Aut': 'jose' 
    }





@app.route('/protected', methods=['GET'])
@jwt_required
def protected():
    # Access the identity of the current user with get_jwt_identity
    current_user = get_jwt_identity()
    return jsonify(logged_in_as=current_user), 200


######################## Import resource ################################################

# controllers cliente empresa -- 01
api.add_resource(ControllerPrueba, '/prueba')
api.add_resource(ControllerUserTocken, '/user_tocken')
api.add_resource(ControllerCliente, '/cliente')
# api.add_resource(ControllerImage, '/imagen')
api.add_resource(ControllerClienteAccess, '/cliente_access_change')

# user controllersuser
api.add_resource(ControllerUserValid, '/validar_login')
api.add_resource(ControllerUserLogout '/logout')

# validar acceso usuario admin - ELENA
api.add_resource(ControllerAcceso, '/login_usuario')



# consultas estaticas Master
# api.add_resource(ControllerPais, '/paises')
# ControllerDepartamento, ControllerProvincia, ControllerDistrito, ControllerMoneda, 
# ControllerTipoContacto, ControllerTipoCambio, ControllerPaquetes, ControllerUnidaSunat

api.add_resource(ControllerUnidaSunat, '/tipounidadsunat')
api.add_resource(ControllerDepartamento, '/departamento')
api.add_resource(ControllerProvincia, '/provincia')
api.add_resource(ControllerDistrito, '/distrito')
api.add_resource(ControllerMoneda, '/moneda')
api.add_resource(ControllerTipoContacto, '/tipocontacto')
api.add_resource(ControllerTipoCambio, '/tipocambio')
api.add_resource(ControllerPaquetes, '/paquetes')



#perfil
api.add_resource(Perfil, '/perfil')

api.add_resource(ControllerDashboard, '/dashboard') #Said 16/05/2018

#Menu Administracion
    #Vehiculo - Said 13/03/2018
api.add_resource(ControllerVehiculoListar, '/vehiculo_listar')
api.add_resource(ControllerVehiculoFiltrar, '/vehiculo_filtrar')
api.add_resource(ControllerVehiculo, '/vehiculo')
    #Conductor - Said 13/03/2018
api.add_resource(ControllerConductorListar, '/conductor_listar')
api.add_resource(ControllerConductorFiltrar, '/conductor_filtrar')
api.add_resource(ControllerConductor, '/conductor')
    #Condicion Pago - Said 13/03/2018
api.add_resource(ControllerCondicionPagoListar, '/condicion_pago_listar')
api.add_resource(ControllerCondicionPagoFiltrar, '/condicion_pago_filtrar')
api.add_resource(ControllerCondicionPago, '/condicion_pago')
    #PerfilAdm - Said 14/03/2018
api.add_resource(ControllerPerfilAdmListar, '/perfiladm_listar')
api.add_resource(ControllerPerfilAdmFiltrar, '/perfiladm_filtrar')
api.add_resource(ControllerPerfilAdm, '/perfiladm')
    #Usuario - Said 14/03/2018
api.add_resource(ControllerUsuarioListar, '/usuario_listar')
api.add_resource(ControllerUsuarioFiltrar, '/usuario_filtrar')
api.add_resource(ControllerUsuario, '/usuario')
api.add_resource(ControllerLocalesDisponibles, '/locales_disponibles')
    #Unidad Medida
api.add_resource(ControllerUnidadMedida, '/unidad_medida')
api.add_resource(ControllerUnidadMedidaListar, '/unidad_medida_listar') #Said 15/03/2018 
api.add_resource(ControllerUnidadMedidaFiltrar, '/unidad_medida_filtrar') #Said 04/04/2018 
    #Contacto - Said 16/03/2018   
api.add_resource(ControllerContactoListar, '/contacto_listar')
api.add_resource(ControllerContactoxId, '/contacto_filtrar')
api.add_resource(ControllerContacto, '/contacto')
api.add_resource(ControllerContactoValidarDocumento, '/validar_documento')
api.add_resource(ControllerContactoValidarEmail, '/validar_email') 
api.add_resource(ControllerDireccion, '/direccion_contacto') #Said 02/08/2018
api.add_resource(ControllerDireccionListar, '/direccion_contacto_listar') #Said 02/08/2018
    #Producto
api.add_resource(ControllerProductoListar, '/producto_listar') #Said 16/03/2018
api.add_resource(ControllerProductoxId, '/producto_filtrar') #Said 16/03/2018
api.add_resource(ControllerProducto, '/producto')
    #Local
api.add_resource(ControllerLocalListar, '/local_listar') #Said 16/03/2018
api.add_resource(ControllerLocalListar2, '/local_listar2') #Jose 16/03/2018
api.add_resource(ControllerLocalxId, '/local_filtrar') #Said 16/03/2018
api.add_resource(ControllerLocal, '/local')
    #Asignacion
api.add_resource(ControllerAsignacionListar, '/asignacion_listar') #Said 30/04/2018
api.add_resource(ControllerAsignacion, '/asignacion')
api.add_resource(ControllerAsignacionComprobantes, '/asignacion_comprobantes') #Said 04/05/2018


#Configuracion Cliente
    #TemplateFac
api.add_resource(ControllerTemplatefacListar, '/templatefac_listar') #Said 19/03/2018
api.add_resource(ControllerTemplatefacxId, '/templatefac_filtrar') #Said 19/03/2018
api.add_resource(ControllerTemplatefacxCliente, '/templatefac_xcliente') #Said 19/03/2018
api.add_resource(ControllerTemplatefac, '/templatefac') #Said 19/03/2018

    # Cliente
api.add_resource(ControllerClientexId, '/cliente_xid')#Said 19/03/2018, Configuracion de Cliente, muestra informacion del Cliente
    # Datos agregados por jose Cliente - Empresa
api.add_resource(ControllerInfoBasico, '/cliente_basico') # Jose actualizamos los datos del cliente
api.add_resource(ControllerUsuarioWeb, '/userweb')#Said 19/03/2018, Actualiza contraseña del Usuario Web Cliente

api.add_resource(ControllerLogoTemplate, '/cliente_ltemp') # Jose actualizacion del logo y de la plantilla
api.add_resource(ControllerLogoUpload, '/cliente_ulogo') # Jose actualizacion del archivo de configuración
api.add_resource(ControllerCertdata, '/cliente_cer_data') # Jose actualizacion los campos decertificado
api.add_resource(ControllerCertUpload, '/cliente_cer_up') # Jose subir archivo de certificado

# Usuario configuracion -- Jose
api.add_resource(ControllerCinfUsuxId, '/usuario_xid_g') # Jose 9/05/2018, consultar dtos del usuario configuracion
api.add_resource(ControllerCdatauser, '/usuario_data') # Jose 9/05/2018, actualizacion los campos de usuario cabecera
api.add_resource(ControllerCUsupasWeb, '/usuario_upcontra') # Jose 9/05/2018, actualizar datos usuario contraseña

#Configuracion Usuario     Said 19/03/2018
    #api.add_resource(ControllerUsuario, '/usuario') usar el Metodo PUT para actualizar contraseña.


#Utiles para Comprobantes
api.add_resource(ControllerUtilContactoTipo, '/util_tipocontacto') #Said 20/03/2018
api.add_resource(ControllerUtilSerieCorrelativo, '/util_seriecorrelativo') #Said 20/03/2018
api.add_resource(ControllerTipoAfectoIGV, '/util_tipoafectoigv') #Said 20/03/2018
api.add_resource(ControllerUtilProductoxMoneda, '/util_prodxmoneda') #Said 20/03/2018
api.add_resource(ControllerUtilUnidadMedida, '/util_unidadmedida') #Said 20/03/2018
api.add_resource(ControllerComprobanteaModificar, '/util_comprobmod') #Said 22/03/2018
api.add_resource(ControllerTipoNotaCredito, '/util_tipo_notacredito') #Said 22/03/2018
api.add_resource(ControllerTipoNotaDebito, '/util_tipo_notadebito') #Said 22/03/2018
api.add_resource(ControllerTipoOperacion, '/util_tipo_operacion') #Said 22/03/2018
api.add_resource(ControllerRegimenPercepcion, '/util_regimen_percepcion') #Said 27/03/2018
api.add_resource(ControllerRegimenRetencion, '/util_regimen_retencion') #Said 27/03/2018
api.add_resource(ControllerMotivoTraslado, '/util_motivo_trasl') #Said 27/03/2018
api.add_resource(ControllerModalidadTraslado, '/util_modalidad_trasl') #Said 27/03/2018
api.add_resource(ControllerTipoTransporte, '/util_tipo_transporte') #Said 03/04/2018
api.add_resource(ControllerComprobanteBaja, '/util_comprobante_baja') #Said 10/04/2018
api.add_resource(ControllerUtilCondicionPago, '/util_condicionpago') #Said 27/04/2018
api.add_resource(ControllerUtilPerfiles, '/util_perfiles') #Said 27/04/2018
api.add_resource(ControllerUtilConsultarComprobante, '/util_consultar_comprobante') #Said 02/05/2018
api.add_resource(ControllerUtilEnviarEmailComprobante, '/util_consultar_contacto_em') #Jose 05/05/2018
api.add_resource(ControllerUtilEnviarEmailComprobanteEnv, '/util_consultar_contacto_env') #Jose 05/05/2018
api.add_resource(ControllerUtilValidarSerieCorrelativo, '/util_validar_seriecorrelativo') #Said 21/05/2018 
api.add_resource(ControllerUtilClientesConRUc, '/util_clientes_conruc') #Said 11/06/2018

#Opciones Para los PerfilesAdm
api.add_resource(ControllerOpcionesAdm, '/opcionesadm')

# Cotizacion
api.add_resource(ControllerCotizacionListar, '/cotizacion_listar') #Said 21/03/2018
api.add_resource(ControllerCotizacionFiltrar, '/cotizacion_filtrar') #Said 21/03/2018
api.add_resource(ControllerCotizacion, '/cotizacion') #Said 21/03/2018
api.add_resource(ControllerCotizacionBusqueda, '/cotizacion_busqueda') #Said 08/05/2018
api.add_resource(ControllerCotizacionBusquedaPagina, '/cotizacion_busqueda_pag') #Said 08/05/2018
api.add_resource(ControllerCotizacionPreview, '/cotizacion_preview') #Said 22/05/2018
api.add_resource(ControllerCotizacionFiltroTotal, '/cotizacion_filtro_total') #Said 20/06/2018
api.add_resource(ControllerCotizacionSeguimiento, '/cotizacion_seguimiento') #Said 03/07/2018

# Factura
# api.add_resource(ControllerFacturaListar, '/factura_listar') #Said 22/03/2018
# api.add_resource(ControllerFacturaFiltrar, '/factura_filtrar') #Said 22/03/2018
api.add_resource(ControllerFactura, '/factura') #Said 22/03/2018
# api.add_resource(ControllerFacturaAnular, '/factura_anular') #Said 11/04/2018
api.add_resource(ControllerFacturaSeguimiento, '/factura_seguimiento') #Said 07/05/2018
api.add_resource(ControllerFacturaBusqueda, '/factura_busqueda') #Said 08/05/2018
api.add_resource(ControllerFacturaBusquedaPagina, '/factura_busqueda_pag') #Said 08/05/2018
api.add_resource(ControllerFacturaPreview, '/factura_preview') #Said 22/05/2018

# Boleta
# api.add_resource(ControllerBoletaListar, '/boleta_listar') #Said 22/03/2018
# api.add_resource(ControllerBoletaFiltrar, '/boleta_filtrar') #Said 22/03/2018
api.add_resource(ControllerBoleta, '/boleta') #Said 22/03/2018
# api.add_resource(ControllerBoletaAnular, '/boleta_anular') #Said 11/04/2018
api.add_resource(ControllerBoletaSeguimiento, '/boleta_seguimiento') #Said 05/05/2018
api.add_resource(ControllerBoletaBusqueda, '/boleta_busqueda') #Said 08/05/2018
api.add_resource(ControllerBoletaBusquedaPagina, '/boleta_busqueda_pag') #Said 08/05/2018
api.add_resource(ControllerBoletaPreview, '/boleta_preview') #Said 22/05/2018

# Nota Credito
api.add_resource(ControllerNotaCreditoListar, '/notacred_listar') #Said 23/03/2018
api.add_resource(ControllerNotaCreditoFiltrar, '/notacred_filtrar') #Said 23/03/2018
api.add_resource(ControllerNotaCredito, '/notacred') #Said 23/03/2018
api.add_resource(ControllerNotaCreditoAnular, '/notacred_anular') #Said 11/04/2018
api.add_resource(ControllerNotaCreditoSeguimiento, '/notacred_seguimiento') #Said 07/05/2018
api.add_resource(ControllerNotaCreditoBusqueda, '/notacred_busqueda') #Said 08/05/2018
api.add_resource(ControllerNotaCreditoBusquedaPagina, '/notacred_busqueda_pag') #Said 08/05/2018
api.add_resource(ControllerNotaCreditoPreview, '/notacred_preview') #Said 22/05/2018

# Nota Debito
api.add_resource(ControllerNotaDebitoListar, '/notadeb_listar') #Said 23/03/2018
api.add_resource(ControllerNotaDebitoFiltrar, '/notadeb_filtrar') #Said 23/03/2018
api.add_resource(ControllerNotaDebito, '/notadeb') #Said 23/03/2018
api.add_resource(ControllerNotaDebitoAnular, '/notadeb_anular') #Said 11/04/2018
api.add_resource(ControllerNotaDebitoSeguimiento, '/notadeb_seguimiento') #Said 07/05/2018
api.add_resource(ControllerNotaDebitoBusqueda, '/notadeb_busqueda') #Said 08/05/2018
api.add_resource(ControllerNotaDebitoBusquedaPagina, '/notadeb_busqueda_pag') #Said 08/05/2018
api.add_resource(ControllerNotaDebitoPreview, '/notadeb_preview') #Said 22/05/2018

# Comprobante Percepcion
api.add_resource(ControllerPercepcion, '/percep') #Said 26/03/2018
api.add_resource(ControllerPercepcionAnular, '/percep_anular') #Said 19/04/2018
api.add_resource(ControllerPercepcionSeguimiento, '/percep_seguimiento') #Said 07/05/2018
api.add_resource(ControllerPercepcionBusqueda, '/percep_busqueda') #Said 08/05/2018
api.add_resource(ControllerPercepcionPreview, '/percep_preview') #Said 22/05/2018

# Comprobante Retencion
api.add_resource(ControllerRetencion, '/reten') #Said 27/03/2018
api.add_resource(ControllerRetencionSeguimiento, '/reten_seguimiento') #Said 07/05/2018
api.add_resource(ControllerRetencionBusqueda, '/reten_busqueda') #Said 08/05/2018
api.add_resource(ControllerRetencionPreview, '/reten_preview') #Said 22/05/2018

# Guia Remision Remitente
api.add_resource(ControllerGuiaRemision, '/gremision') #Said 28/03/2018
api.add_resource(ControllerGuiaRemisionSeguimiento, '/gremision_seguimiento') #Said 11/05/2018
api.add_resource(ControllerGuiaRemisionBusqueda, '/gremision_busqueda') #Said 11/05/2018
api.add_resource(ControllerGuiaRemisionPreview, '/gremision_preview') #Said 20/07/2018
api.add_resource(ControllerDireccionesListar, '/gremision_direcciones') #Said 08/08/2018
api.add_resource(ControllerDocumentosRelacionados, '/gremision_documentos') #Said 14/08/2018

# Guia Remision Transportista
# api.add_resource(ControllerGuiaTransporteListar, '/gtransporte_listar') #Said 28/03/2018
# api.add_resource(ControllerGuiaTransporteFiltrar, '/gtransporte_filtrar') #Said 28/03/2018
# api.add_resource(ControllerGuiaTransporte, '/gtransporte') #Said 28/03/2018

# Documento de Baja y Reversiones
api.add_resource(ControllerDocBajaListar, '/docbaja_listar') #Said 12/04/2018
api.add_resource(ControllerDocReversionListar, '/docreversion_listar') #Said 19/04/2018

# Comunicacio de Baja
api.add_resource(ControllerComunicacion, '/comunicacion') #Said 16/04/2018
api.add_resource(ControllerComunicacionListar, '/comunicacion_listar') #Said 16/04/2018
api.add_resource(ControllerComunicacionDetListar, '/detcomunicacion_listar') #Said 16/04/2018
api.add_resource(ControllerLoteListar, '/lote_listar') #Said 16/04/2018

# Resumen de boletas
api.add_resource(ControllerResumenBoleta, '/resumenbol') #Said 18/04/2018
api.add_resource(ControllerResumenBoletaListar, '/resumenbol_listar') #Said 18/04/2018
api.add_resource(ControllerResumenBoletaDetListar, '/detresumenbol_listar') #Said 18/04/2018
api.add_resource(ControllerLoteResumenListar, '/loteresumen_listar') #Said 18/04/2018
api.add_resource(ControllerPendientesResumenListar, '/pendiente_resumenbol') #Said 18/04/2018

#Anular comprobantes
api.add_resource(ControllerAnulacion, '/anular')

# Descargar archvio PDF, XML, CDR
api.add_resource(ControllerDescargarArhivo, '/descargar_archivo')
api.add_resource(ControllerDescargarArhivojose, '/file/<ruc>/<comprobante>/<fecha>/<namefile>/<ext>')

# Busqueda Avanzada
api.add_resource(ControllerBusquedaAvanzada, '/busqueda') #Said 09/05/2018
api.add_resource(ControllerBusquedaAvanzadaPagina, '/busqueda_pag') #Said 09/05/2018

# Recuperacion de contrasenia
api.add_resource(ControllerRecuperarContrasenia, '/recuperar_pas') #Said 09/05/2018
api.add_resource(ControllerValidaparamContrasenia, '/valida_param') #Jose 12/06/2018
api.add_resource(ControllerSaveparamContrasenia, '/save_param') #Jose 12/06/2018

# Reporte para el dashboard de Elena
api.add_resource(ControllerReporte, '/reporte') #Said 16/05/2018
api.add_resource(ControllerListarClientes, '/eclientes') #Said 01/06/2018
api.add_resource(ControllerListarUsuario, '/eusuarios') #Said 06/07/2018

#Reportes
api.add_resource(ControllerReportes, '/reporte_comprobantes') #Said 22/05/2018
api.add_resource(ControllerReportesUtilLocal, '/util_reporte_local') #Said 23/05/2018
api.add_resource(ControllerReportesUtilUsuario, '/util_reporte_usuario') #Said 23/05/2018
api.add_resource(ControllerRptFacturasFechaPago, '/reporte_fac_xfecha_pago') #Said 15/06/2018
api.add_resource(ControllerReportesExcel, '/reporte_comprobantes/<cliente>/<comprobante>/<moneda>/<desde>/<hasta>/<usuario>/<local>') #Said 02/07/2018

#pdf ct 
api.add_resource(ControllerCotizacionPdf, '/cotizacion_pdf/<cotizacion>') #BRUNO 06/06/2018
api.add_resource(ControllerCotizacionPdf2, '/cotizacion_pdf2/<cotizacion>') #SAID 12/06/2018

# Dashboard
api.add_resource(ControllerDashboard2, '/dashboard_v2') #Said 08/06/2018

#Simple - Para procesos manuales
api.add_resource(ControllerClientexRuc, '/simple_clientexruc') #Said 14/06/2018

#Excel
api.add_resource(ControllerExecelPrueba, '/excel_prueba') #Said 02/07/2018

#Servicios
api.add_resource(ControllerServicioFactura, '/servicio_factura') #Said 12/07/2018
api.add_resource(ControllerServicioBoleta, '/servicio_boleta') #Said 13/07/2018
api.add_resource(ControllerServicioNotaCredito, '/servicio_notacredito') #Said 13/07/2018
api.add_resource(ControllerServicioNotaDebito, '/servicio_notadebito') #Said 13/07/2018
api.add_resource(ControllerServicioGuiaRemision, '/servicio_guiaremision') #Said 20/07/2018
api.add_resource(ControllerServicioPercepcion, '/servicio_percepcion') #Said 15/08/2018
api.add_resource(ControllerServicioRetencion, '/servicio_retencion') #Said 21/08/2018

#Consulta Publica
api.add_resource(ControllerConsultaPublica, '/ruc/<ruc>') #Said 24/07/2018
api.add_resource(ControllerConsultaPublicaComprobantes, '/comprobantes') #Said 24/07/2018
api.add_resource(ControllerConsultaPublicaInfo, '/consulta') #Said 24/07/2018

#Cobranza
api.add_resource(ControllerCobranzaListar, '/cobranza_listar') #Said 25/07/2018
api.add_resource(ControllerCobranzaPreview, '/cobranza_preview') #Said 25/07/2018
api.add_resource(ControllerCobranza, '/cobranza') #Said 26/07/2018
api.add_resource(ControllerCobranzaNotificacion, '/cobranza_notificacion') #Said 01/08/2018
api.add_resource(ControllerFormaPago, '/formapago') #Said 26/07/2018

api.add_resource(ControllerPrueba2, '/pruebajose') #Said 26/07/2018

if __name__ == '__main__':
    # app.run('192.168.1.50', debug=True, port=80)
    app.run('192.168.21.123', debug=True, port=80)
    # app.run('192.168.1.26', debug=True)
    # app.run(debug=True)